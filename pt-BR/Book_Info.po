#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2020-05-17 17:54+0200\nPO-Revision-Date: 2021-05-03 21:32+0000\nLast-Translator: Fred Maranhão <fred.maranhao@gmail.com>\nLanguage-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/debian-handbook/book_info/pt_BR/>\nLanguage: pt-BR\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n > 1;\nX-Generator: Weblate 4.7-dev\n"

msgid "The Debian Administrator's Handbook"
msgstr "O Manual do(a) Administrador(a) Debian"

msgid "Debian Buster from Discovery to Mastery"
msgstr "Debian Buster, da Descoberta à Maestria"

msgid "Debian"
msgstr "Debian"

msgid "A reference book presenting the Debian distribution, from initial installation to configuration of services."
msgstr "Um livro de referência apresentando a distribuição Debian, da instalação inicial até a configuração de serviços."

msgid "ISBN: 979-10-91414-19-7 (English paperback)"
msgstr "ISBN: 979-10-91414-19-7 (Brochura em inglês)"

msgid "ISBN: 979-10-91414-20-3 (English ebook)"
msgstr "ISBN: 979-10-91414-20-3 (E-book em inglês)"

msgid "This book is available under the terms of two licenses compatible with the Debian Free Software Guidelines."
msgstr "Este livro está disponível sob os termos de duas licenças compatíveis com a Definição Debian de Software Livre."

msgid "Creative Commons License Notice:"
msgstr "Nota da Licença Creative Commons:"

msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"https://creativecommons.org/licenses/by-sa/3.0/\" />"
msgstr "Este livro está licenciado sob uma Licença Creative Commons Attribution-ShareAlike 3.0 Unported. <ulink type=\"block\" url=\"http://creativecommons.org/licenses/by-sa/3.0/\" />"

msgid "GNU General Public License Notice:"
msgstr "Nota da Licença Pública Geral da GNU:"

msgid "This book is free documentation: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version."
msgstr "Este livro é documentação livre; você pode redistribuí-lo e/ou modificá-lo dentro dos termos da Licença Pública Geral GNU como publicada pela Fundação do Software Livre, tanto na versão 2 da Licença, ou (por opção sua) qualquer versão posterior."

msgid "This book is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
msgstr "Este livro é distribuído na esperança de que ele seja útil, mas SEM QUALQUER GARANTIA; nem mesmo a garantia implícita de COMERCIALIZAÇÃO ou ADEQUAÇÃO PARA UM PROPÓSITO PARTICULAR. Veja a Licença Pública Geral GNU para mais detalhes."

msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"https://www.gnu.org/licenses/\" />."
msgstr "Você deve ter recebido uma cópia da Licença Pública Geral GNU junto com este programa. Se não, veja <ulink url=\"https://www.gnu.org/licenses/\" />."

msgid "Show your appreciation"
msgstr "Mostre sua apreciação"

msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
msgstr "Este livro é publicado sob uma licença livre porque queremos que todos se beneficiem dele. Dito isto, mantê-lo requer tempo e muito esforço, e nós apreciamos receber agradecimentos por isto. Se você achar este livro valioso, por favor, considere contribuir para sua contínua manutenção, seja através da compra do livro ou fazendo uma doação através do site oficial do livro: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
