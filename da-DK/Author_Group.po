#
# AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2015-09-09 18:47+0200\nPO-Revision-Date: 2021-06-28 14:32+0000\nLast-Translator: Petter Reinholdtsen <pere-weblate@hungry.com>\nLanguage-Team: Danish <https://hosted.weblate.org/projects/debian-handbook/author_group/da/>\nLanguage: da-DK\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n != 1;\nX-Generator: Weblate 4.7.1-dev\n"

msgid "Raphaël"
msgstr "Raphaël"

msgid "Hertzog"
msgstr "Hertzog"

msgid "Roland"
msgstr "Roland"

msgid "Mas"
msgstr "Mas"
